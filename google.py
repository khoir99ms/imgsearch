__author__ = 'Khoir MS'

import re
import time

from browser import Browser
from extractor import GSearch as EXTGsearch

class Google:

    def __init__(self, driver=None, **kwargs):
        self.driver = driver
        if not self.driver:
            self.driver = Browser(**kwargs)
        self.gsearch = EXTGsearch()

        self.baselink = 'https://images.google.com/'
        self.__locator_searchfield = '#qbi'
        self.__locator_searchfield_image_tab_url = '//a[contains(text(), \'Paste image URL\')]'
        self.__locator_searchfield_image_tab_file = '//a[contains(text(), \'Upload an image\')]'

        self.__locator_searchfield_image_by_url = 'input[name=image_url]'
        self.__locator_searchfield_image_by_file = 'input[name=encoded_image]'

        self.__locator_similar_image = 'a.iu-card-header'

        self.__locator_pager = '#foot td:not(.navend)'

    def search(self, url=None, file=None, image=False, page=0):
        result = {'success': False}
        try:
            if self.driver.browser.current_url != self.baselink:
                self.driver.load_and_wait(url=self.baselink, selector=self.__locator_searchfield, wait=3)
            search = self.driver.get_element(selector=self.__locator_searchfield)

            if search:
                search.click()

            if url:
                self.driver.log('search by url')
                tab_url = self.driver.wait_by_xpath(selector=self.__locator_searchfield_image_tab_url)
                if tab_url and tab_url.is_displayed():
                    tab_url.click()

                input_url = self.driver.get_element(selector=self.__locator_searchfield_image_by_url)
                if input_url:
                    input_url.clear()
                    self.driver.log('send keys parameter')
                    input_url.send_keys(url)
                    self.driver.press_enter(input_url)
                    self.driver.wait_by_css_selector(selector='#resultStats', wait=10)

            if file:
                self.driver.log('search by uploading file')
                tab_file =  self.driver.wait_by_xpath(selector=self.__locator_searchfield_image_tab_file)
                if tab_file and tab_file.is_displayed():
                    tab_file.click()

                input_file = self.driver.get_element(selector=self.__locator_searchfield_image_by_file)
                if input_file:
                    self.driver.log('send keys parameter')
                    input_file.send_keys(file)
                    self.driver.wait_by_css_selector(selector='#resultStats', wait=10)

            if page and not image:
                retry = 0
                max_retry = 100
                while True:
                    if page > 10:
                        pages = self.driver.get_elements(selector='{} > a'.format(self.__locator_pager))
                        if pages:
                            self.driver.click(pages[-1])
                            time.sleep(1)

                    page_n = self.driver.get_element(selector='{} > a[aria-label="Page {}"]'.format(self.__locator_pager, page))
                    if page_n:
                        self.driver.click(page_n)
                        time.sleep(0.5)
                        break
                    else:
                        curpage = self.driver.get_element(selector='#foot td:not(.navend).cur')
                        if curpage:
                            curpage = self.driver.get_value(curpage)
                            curpage = re.sub(r'[^0-9]', '', curpage, flags=re.U)
                            curpage = curpage.strip()
                            if str(curpage) == str(page):
                                self.driver.log('page {} is current page'.format(page))
                                break

                        if retry > max_retry:
                            break
                        retry += 1
                        self.driver.log('retry to {} for get page {}'.format(retry, page))

            if image:
                similar_link = self.driver.get_element(selector=self.__locator_similar_image)
                if similar_link:
                    similar_link.click()
                    self.driver.wait_by_css_selector(selector='#center_col', wait=3)

                times = 1
                last_try_to_scroll = 0
                try_to_scroll_limit = 3
                while True:
                    last_height = int(self.driver.browser.execute_script("return document.body.scrollHeight;"))
                    self.driver.log('scroll times {}'.format(times))
                    self.driver.scroll_down()
                    time.sleep(0.5)

                    current_height = int(self.driver.browser.execute_script("return document.body.scrollHeight;"))
                    if last_height == current_height:
                        if last_try_to_scroll < try_to_scroll_limit:
                            self.driver.log('increase try to scroll')
                            last_try_to_scroll += 1
                        else:
                            break
                    else:
                        last_try_to_scroll = 0
                    times += 1

            html = self.driver.get_element('html').get_attribute('outerHTML')
            self.gsearch.set_document(html)
            if image:
                sresult = self.gsearch.images()
            else:
                sresult = self.gsearch.search()

            result['data'] = sresult
            result['total'] = len(sresult)
            result['success'] = True
        except:
            self.driver.log(level='error')
            result['data'] = list()
            result['total'] = 0

        return result

if __name__ == '__main__':
    import json
    gs = Google()
    # gs = Google(driver='chrome', driver_path='/home/khoir/Workspace/chromedriver')
    # print json.dumps(gs.search(url='https://www.tes.com/sites/default/files/styles/news_article_hero/public/getting-started-as-an-author-on-tes.png?itok=6SB5jEIF'), indent=4)
    print json.dumps(gs.search(file='/home/khoir/Pictures/windows_xp_bliss-wide.jpg', image=True), indent=4)
    # print json.dumps(gs.search(url='https://images.pexels.com/photos/66997/pexels-photo-66997.jpeg?w=940&h=650&auto=compress&cs=tinysrgb', image=True), indent=4)