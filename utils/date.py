__author__ = 'Khoir MS'

import re
import pytz
import time
import dateparser
import timelib
import calendar

from datetime import datetime
from dateutil.parser import parse

def convert_todate(date, to_local=False, to_utc=False):
    try:
        result = strtotime(date)
        result = is_valid_date(result, to_local, to_utc)
    except Exception:
        raise Exception("failed to convert date by both converter.")

    return result

def strtotime(date):
    if isinstance(date, float): date = int(date)

    date = re.sub(r'[|]', ' ', str(date))
    date = re.sub(r'[ ]+', ' ', date).strip()

    try:
        try:
            result = parse(date)
        except Exception:
            # checking date is timestamp
            if re.search(r'^[\d\.]+$', date):
                date_timestamp = int(date)
            else:
                date_timestamp = timelib.strtotime(date)
            result = datetime.fromtimestamp(date_timestamp)
    except:
        raise
    return result

def timezone():
    if time.daylight:
        offsetHour = time.altzone / 3600
    else:
        offsetHour = time.timezone / 3600
    localTZ = 'Etc/GMT%+d' % offsetHour
    tz = pytz.timezone(localTZ)

    return tz

def is_valid_date(date_compare, to_local=False, to_utc=False):
    fmt = '%Y-%m-%d %H:%M:%S'
    comparator = datetime.now()

    try:
        if isinstance(date_compare, datetime):
            date_compare = date_compare.strftime(fmt)
            date_compare = datetime.strptime(date_compare, fmt)
            if date_compare > comparator:
                date_compare = comparator
            else:
                date_compare = date_compare

            # add timezone
            if to_local:
                date_compare = utc_to_local(date_compare)
                date_compare = date_compare.replace(tzinfo=timezone())
            if to_utc:
                date_compare = local_to_utc(date_compare)

            if not to_local: date_compare = date_compare.replace(tzinfo=pytz.utc)

            # date_compare = date_compare.strftime('{}{}'.format(fmt, fmt_tz))
        else:
            raise Exception("Value not instance from datetime")
    except Exception:
        raise

    return date_compare

def local_to_utc(t):
    # input datetime obj or timestamp
    if re.search(r'^[\d\.]+$', str(t)):
        secs = int(t)
    else:
        secs = time.mktime(t.timetuple())
    struct_time = time.gmtime(secs)

    return datetime.fromtimestamp(time.mktime(struct_time))

def utc_to_local(t):
    # input datetime obj or timestamp
    if re.search(r'^[\d\.]+$', str(t).strip()):
        secs = int(t)
    else:
        secs = calendar.timegm(t.timetuple())
    struct_time = time.localtime(secs)

    return datetime.fromtimestamp(time.mktime(struct_time))

def datetime_to_timestamp(dt):
    if isinstance(dt, basestring): dt = convert_todate(dt)

    return time.mktime(dt.timetuple())

if __name__=='__main__':
    print linkedin_date_fixer('1m')