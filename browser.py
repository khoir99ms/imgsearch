import time
import random
import atexit

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from logger import Logger
from utils.html import html_entities

class Browser(Logger, object):
    __DRIVER = {"path": "phantomjs", "name": "phantomjs"}
    __SERVICE_LOG_PATH = "/dev/null"

    __UAGENT = [
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0',
        'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/55.0.2883.87 Chrome/55.0.2883.87 Safari/537.36'
    ]

    def __init__(self, **kwargs):
        super(Browser, self).__init__()

        self.WAIT = 1

        self.driver = kwargs.pop('driver', '')
        self.driver_path = kwargs.pop('driver_path', '')

        if self.driver:
            self.__DRIVER["name"] = self.driver
        if self.driver_path:
            self.__DRIVER["path"] = self.driver_path

        if self.__DRIVER['name'] == 'phantomjs':
            random.shuffle(self.__UAGENT)
            dcap = DesiredCapabilities.PHANTOMJS.copy()
            dcap['phantomjs.page.settings.userAgent'] = random.choice(self.__UAGENT)
            # dcap['phantomjs.page.settings.clearMemoryCaches'] = True
            # accept untrusterd certs
            service_args = []
            # service_args = ['--web-security', 'false', '--ignore-ssl-errors', 'yes']
            self.browser = webdriver.PhantomJS(desired_capabilities=dcap, executable_path=self.__DRIVER['path'], service_args=service_args, service_log_path=self.__SERVICE_LOG_PATH)
        elif self.__DRIVER['name'] == 'firefox':
            self.browser = webdriver.Firefox()
        elif self.__DRIVER['name'] == 'chrome':
            self.browser = webdriver.Chrome(executable_path=self.__DRIVER['path'], service_log_path=self.__SERVICE_LOG_PATH)
        else:
            raise Exception('no driver found')

        w_width,w_height = 1024, 800
        atexit.register(self.quit)
        self.browser.set_window_size(width=w_width, height=w_height)

        self.log('initialize ... ')
        self.log('using driver {}'.format(self.__DRIVER['name']))
        self.log('set window size {} x {}'.format(w_width, w_height))

    def set_proxies(self, proxy, port):
        self.browser.command_executor._commands['executePhantomScript'] = ('POST', '/session/$sessionId/phantom/execute')
        self.browser.execute('executePhantomScript', {'script': '''phantom.setProxy("{}", {});'''.format(proxy, port), 'args': []})

    def load_page(self, page):
        try:
            self.log('opening page {}'.format(page))
            self.browser.get(page)
            return True
        except:
            return False

    def submit_form(self, element):
        try:
            element.submit()
            return True
        except TimeoutException:
            return False

    def get_element_from(self, object, selector):
        try:
            return object.find_element_by_css_selector(selector)
        except NoSuchElementException:
            return None

    def get_elements_from(self, object, selector):
        try:
            return object.find_elements_by_css_selector(selector)
        except NoSuchElementException:
            return []

    def get_element(self, selector):
        return self.get_element_from(self.browser, selector)

    def get_elements(self, selector):
        return self.get_elements_from(self.browser, selector)

    def get_element_from_value(self, object, selector):
        element = self.get_element_from(object, selector)
        return self.get_value(element)

    def get_element_value(self, selector):
        element = self.get_element(selector)
        return self.get_value(element)

    def get_value(self, element):
        if element:
            return element.text
        return None

    def get_attribute(self, element, attribute):
        if element:
            return element.get_attribute(attribute)
        return None

    def get_element_from_attribute(self, fromObject, selector, attribute):
        element = self.get_element_from(fromObject, selector)
        return self.get_attribute(element, attribute)

    def get_element_attribute(self, selector, attribute):
        element = self.get_element(selector)
        return self.get_attribute(element, attribute)

    def get_parent_levels(self, node, levels):
        path = '..'
        if levels > 1:
            for i in range(1, levels):
                path = path + '/..'
        return node.find_element_by_xpath(path)

    def get_parent_node(self, node):
        return node.find_element_by_xpath('..')

    def get_child_nodes(self, node):
        return node.find_elements_by_xpath('./*')

    def select_and_write(self, field, value):
        fieldObject = self.get_element(field)
        fieldObject.send_keys(value)
        return fieldObject

    def press_enter(self, object):
        object.send_keys(Keys.RETURN)
        return object

    def click_selector(self, selector, parent=None):
        element = self.get_element(selector)
        container = self.get_element(parent) if parent else None

        if element:
            try:
                self.click(element, container)
                return True
            except Exception as e:
                print e
                return False
        return False

    def click_multiple(self, selector):
        exit = False
        elements = self.get_elements(selector)
        if elements:
            for element in elements:
                try:
                    self.click(element)
                    exit = True
                except:
                    pass
        return exit

    def click_next(self, selector, limit=0, wait=0):
        exit = False
        if not wait: wait = self.WAIT
        wdw = WebDriverWait(self.browser, wait)

        n = 0
        while True:
            try:
                if limit and n >= limit: break

                element = wdw.until(EC.element_to_be_clickable((By.CSS_SELECTOR, selector)))
                self.click(element)
                exit = True

                n +=1
            except TimeoutException:
                break  # cannot click the button anymore
        time.sleep(0.5)

        return exit

    def click(self, element, container=None):
        self.log('click element {} {}'.format(element.tag_name, element))
        if not container: container = element
        actions = webdriver.ActionChains(self.browser)
        actions.move_to_element(container)
        actions.click(element)
        actions.perform()

    def move_to_element(self, element):
        self.browser.execute_script("return arguments[0].scrollIntoView();", element)
        actions = webdriver.ActionChains(self.browser)
        actions.move_to_element(element)
        actions.perform()

    def scroll_down(self):
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    def scrolling_down(self, times):
        for i in range(1, times):
            self.scroll_down()
            time.sleep(0.5)

    def scrolling_down_until(self, selector):
        times = 1
        last_try_to_scroll = 0
        try_to_scroll_limit = 3
        while not self.wait_by_css_selector(selector=selector, wait=1):
            last_height = int(self.browser.execute_script("return document.body.scrollHeight;"))
            self.log('scroll times {}'.format(times))
            self.scroll_down()
            time.sleep(0.5)

            current_height = int(self.browser.execute_script("return document.body.scrollHeight;"))
            if last_height == current_height:
                if last_try_to_scroll < try_to_scroll_limit:
                    self.log('increase try to scroll')
                    last_try_to_scroll += 1
                else:
                    break
            else:
                last_try_to_scroll = 0
            times += 1

    def scrolling_to_element(self, selector, retry=1):
        times = 0
        while times < retry:
            try:
                self.log('scroll times {}'.format(times))
                element = self.browser.find_element_by_css_selector(selector)
                self.browser.execute_script("return arguments[0].scrollIntoView();", element)
                time.sleep(0.5)

                return True
            except:
                pass
            times += 1

        return False

    def slow_scroll(self, interval=None, wait=1, limit=0):
        if interval is None:
            window = self.browser.get_window_size()
            interval = window['height']*0.8

        height = int(self.browser.execute_script("return document.body.scrollHeight;"))
        scroll = interval
        while height > interval and interval:
            self.browser.execute_script("window.scrollTo(0, {});".format(scroll))
            time.sleep(wait)
            scroll += interval
            height -= interval

    def get_field_value(self, record, parent=None):
        exit = None
        if parent:
            if record['type'] == 'attr':
                if record['selector']:
                    exit = self.get_element_from_attribute(parent, record['selector'], record['attr'])
                else:
                    exit = self.get_attribute(parent, record['attr'])
            elif record['type'] == 'text':
                if record['selector']:
                    exit = self.get_element_from_value(parent, record['selector'])
                else:
                    exit = self.get_value(parent)
            elif record['type'] == 'style':
                exit = record['attr']
        else:
            if record['type'] == 'attr':
                exit = self.get_element_attribute(record['selector'], record['attr'])
            elif record['type'] == 'text':
                exit = self.get_element_value(record['selector'])
            elif record['type'] == 'style':
                exit = record['attr']
        if record['type'] == 'text' and exit:
            exit = html_entities(exit)

        return exit

    def load_and_wait(self, url, selector, wait=0):
        if not wait: wait = self.WAIT
        self.load_page(url)

        return self.wait_locate_element(selector, wait)

    def wait_locate_element(self, using, selector, wait=0):
        if wait: self.WAIT = wait

        try:
            wait = WebDriverWait(self.browser, self.WAIT)
            element = wait.until(EC.presence_of_element_located((using, selector)))
            return element
        except:
            return None

    def wait_show_element(self, using, selector, wait=0):
        if wait: self.WAIT = wait

        try:
            return WebDriverWait(self.browser, self.WAIT).until(
                EC.visibility_of_element_located((using, selector))
            )
        except:
            return None

    def wait_hide_element(self, using, selector, wait):
        if not wait: wait = self.WAIT

        try:
            return WebDriverWait(self.browser, wait).until(
                EC.invisibility_of_element_located((using, selector))
            )
        except:
            return None

    def wait_click_element(self, using, selector, wait):
        if not wait: wait = self.WAIT

        try:
            return WebDriverWait(self.browser, wait).until(
                EC.element_to_be_clickable((using, selector))
            )
        except:
            return None

    def wait_by_css_selector(self, selector, what='locate', wait=None):
        if what == 'show':
            return self.wait_show_element(By.CSS_SELECTOR, selector, wait)
        elif what == 'hide':
            return self.wait_hide_element(By.CSS_SELECTOR, selector, wait)
        elif what == 'click':
            return self.wait_click_element(By.CSS_SELECTOR, selector, wait)
        else:
            return self.wait_locate_element(By.CSS_SELECTOR, selector, wait)

    def wait_by_xpath(self, selector, what='locate', wait=None):
        if what == 'show':
            return self.wait_show_element(By.XPATH, selector, wait)
        elif what == 'hide':
            return self.wait_hide_element(By.XPATH, selector, wait)
        elif what == 'click':
            return self.wait_click_element(By.XPATH, selector, wait)
        else:
            return self.wait_locate_element(By.XPATH, selector, wait)

    def wait_by_tag(self, selector, what='locate', wait=None):
        if what == 'show':
            return self.wait_show_element(By.TAG_NAME, selector, wait)
        elif what == 'hide':
            return self.wait_hide_element(By.TAG_NAME, selector, wait)
        elif what == 'click':
            return self.wait_click_element(By.TAG_NAME, selector, wait)
        else:
            return self.wait_locate_element(By.TAG_NAME, selector, wait)

    def wait_by_name(self, selector, what='locate', wait=None):
        if what == 'show':
            return self.wait_show_element(By.NAME, selector, wait)
        elif what == 'hide':
            return self.wait_hide_element(By.NAME, selector, wait)
        elif what == 'click':
            return self.wait_click_element(By.NAME, selector, wait)
        else:
            return self.wait_locate_element(By.NAME, selector, wait)

    def wait_by_link_text(self, selector, what='locate', wait=None):
        if what == 'show':
            return self.wait_show_element(By.LINK_TEXT, selector, wait)
        elif what == 'hide':
            return self.wait_hide_element(By.LINK_TEXT, selector, wait)
        elif what == 'click':
            return self.wait_click_element(By.LINK_TEXT, selector, wait)
        else:
            return self.wait_locate_element(By.LINK_TEXT, selector, wait)

    def wait_by_partial_link_text(self, selector, what='locate', wait=None):
        if what == 'show':
            return self.wait_show_element(By.PARTIAL_LINK_TEXT, selector, wait)
        elif what == 'hide':
            return self.wait_hide_element(By.PARTIAL_LINK_TEXT, selector, wait)
        elif what == 'click':
            return self.wait_click_element(By.PARTIAL_LINK_TEXT, selector, wait)
        else:
            return self.wait_locate_element(By.PARTIAL_LINK_TEXT, selector, wait)

    def wait_by_id(self, selector, what='locate', wait=None):
        if what == 'show':
            return self.wait_show_element(By.ID, selector, wait)
        elif what == 'hide':
            return self.wait_hide_element(By.ID, selector, wait)
        elif what == 'click':
            return self.wait_click_element(By.ID, selector, wait)
        else:
            return self.wait_locate_element(By.ID, selector, wait)

    def wait_by_class_name(self, selector, what='locate', wait=None):
        if what == 'show':
            return self.wait_show_element(By.CLASS_NAME, selector, wait)
        elif what == 'hide':
            return self.wait_hide_element(By.CLASS_NAME, selector, wait)
        elif what == 'click':
            return self.wait_click_element(By.CLASS_NAME, selector, wait)
        else:
            return self.wait_locate_element(By.CLASS_NAME, selector, wait)

    def quit(self):
        self.browser.quit()