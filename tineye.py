__author__ = 'Khoir MS'

import re
import time

from browser import Browser
from extractor import TineyeSearch as EXTTineye

class Tineye:

    def __init__(self, driver=None, **kwargs):
        self.driver = driver
        if not self.driver:
            self.driver = Browser(**kwargs)
        self.tinyeye_search = EXTTineye()

        self.baselink = 'https://www.tineye.com/'

        self.__locator_searchfield = '.search-container'
        self.__locator_searchfield_image_by_url = 'input#url_box'
        # self.__locator_searchfield_image_by_file = 'input#upload_box'
        self.__locator_searchfield_image_by_file = '//input[@id=\'upload_box\']'
        # self.__locator_searchfield_image_by_file = '#upload-button'

        self.__locator_pager = '.pagination-bottom a:not(.next)'
        self.__locator_wait = '.query-summary'
        self.__locator_total_page = '.pagination-top .current + span'

    def search(self, url=None, file=None, page=0, order='asc'):
        result = {'success': False}
        try:
            if self.driver.browser.current_url != self.baselink:
                self.driver.load_and_wait(url=self.baselink, selector=self.__locator_searchfield, wait=3)

            if url:
                self.driver.log('search by url')
                input_url = self.driver.wait_by_css_selector(selector=self.__locator_searchfield_image_by_url, what='show', wait=10)
                if input_url:
                    input_url.clear()
                    self.driver.log('send keys parameter')
                    input_url.send_keys(url)
                    self.driver.press_enter(input_url)
                    self.driver.wait_by_css_selector(selector=self.__locator_wait, wait=3)

            if file:
                self.driver.log('search by uploading file')
                input_file = self.driver.wait_by_xpath(selector=self.__locator_searchfield_image_by_file, wait=10)
                if input_file:
                    self.driver.browser.execute_script("arguments[0].setAttribute('style', 'top:0;position:relative;');", input_file)
                    self.driver.log('send keys parameter')
                    input_file.send_keys(file)
                    self.driver.wait_by_css_selector(selector=self.__locator_wait, wait=3)

            current_url = self.driver.browser.current_url
            sort_url = '{}?sort=crawl_date&order={}'.format(current_url, order)
            self.driver.load_and_wait(url=sort_url, selector=self.__locator_wait, wait=3)

            if page:
                total_page = self.driver.get_element(selector=self.__locator_total_page) or 0
                if total_page:
                    total_page = self.driver.get_value(total_page)
                    total_page = re.sub(r'[^0-9]', '', total_page, flags=re.U)
                    total_page = total_page.strip()
                    total_page = int(total_page)

                if page > total_page:
                    self.driver.log('page set to total page, num page {} greather than total page {}'.format(page, total_page))
                    page = total_page

                page_url = '{}?page={}&sort=crawl_date&order={}'.format(current_url, page, order)
                self.driver.load_and_wait(url=page_url, selector=self.__locator_wait, wait=3)

            html = self.driver.get_element('html').get_attribute('outerHTML')
            self.tinyeye_search.set_document(html)
            sresult = self.tinyeye_search.search()

            result['data'] = sresult
            result['total'] = len(sresult)
            result['success'] = True
        except:
            self.driver.log(level='error')
            result['data'] = list()
            result['total'] = 0

        return result

if __name__ == '__main__':
    import json
    te = Tineye()
    # te = Tineye(driver='chrome', driver_path='/home/khoir/Workspace/chromedriver')
    print json.dumps(te.search(url='https://www.tes.com/sites/default/files/styles/news_article_hero/public/getting-started-as-an-author-on-tes.png?itok=6SB5jEIF'), indent=4)
    # print json.dumps(te.search(file='/home/khoir/Pictures/windows_xp_bliss-wide.jpg'), indent=4)