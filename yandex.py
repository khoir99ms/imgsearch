__author__ = 'Khoir MS'

import re
import time

from browser import Browser
from extractor import YandexSearch as EXTYandexSearch

class Yandex:

    def __init__(self, driver=None, **kwargs):
        self.driver = driver
        if not self.driver:
            self.driver = Browser(**kwargs)
        self.yandex_search = EXTYandexSearch()

        self.baselink = 'https://yandex.com/images/'
        self.__locator_searchfield = '.cbir-logo > button'

        self.__locator_searchfield_image_by_url = 'input[name=cbir-url]'
        self.__locator_searchfield_image_by_file = 'input[name=upfile]'

        self.__locator_similar_image = 'li.similar__thumb_last_yes > a.similar__link'

        self.__locator_wait = 'button.back'

    def search(self, url=None, file=None, image=False):
        result = {'success': False}
        try:
            if self.driver.browser.current_url != self.baselink:
                self.driver.load_and_wait(url=self.baselink, selector=self.__locator_searchfield, wait=3)
            search = self.driver.get_element(selector=self.__locator_searchfield)

            if search:
                search.click()

            if url:
                self.driver.log('search by url')
                input_url = self.driver.wait_by_css_selector(selector=self.__locator_searchfield_image_by_url, what='show', wait=5)
                if input_url:
                    input_url.clear()
                    self.driver.log('send keys parameter')
                    input_url.send_keys(url)
                    self.driver.press_enter(input_url)
                    self.driver.wait_by_css_selector(selector=self.__locator_wait, wait=3)

            if file:
                self.driver.log('search by uploading file')
                input_file = self.driver.wait_by_css_selector(selector=self.__locator_searchfield_image_by_file, what='show', wait=5)
                if input_file:
                    self.driver.log('send keys parameter')
                    input_file.send_keys(file)
                    self.driver.wait_by_css_selector(selector=self.__locator_wait, wait=3)

            if image:
                time.sleep(1.5)
                similar_link = self.driver.get_element(selector=self.__locator_similar_image)
                if similar_link:
                    similar_link.click()
                    self.driver.wait_by_css_selector(selector=self.__locator_wait, wait=5)

            times = 1
            last_try_to_scroll = 0
            try_to_scroll_limit = 3
            while True:
                last_height = int(self.driver.browser.execute_script("return document.body.scrollHeight;"))
                self.driver.log('scroll times {}'.format(times))
                self.driver.scroll_down()
                time.sleep(0.5)

                current_height = int(self.driver.browser.execute_script("return document.body.scrollHeight;"))
                if last_height == current_height:
                    if last_try_to_scroll < try_to_scroll_limit:
                        self.driver.log('increase try to scroll')
                        last_try_to_scroll += 1
                    else:
                        break
                else:
                    last_try_to_scroll = 0
                times += 1
            time.sleep(0.5)

            html = self.driver.get_element('html').get_attribute('outerHTML')
            self.yandex_search.set_document(html)
            if image:
                sresult = self.yandex_search.images()
            else:
                sresult = self.yandex_search.search()

            result['data'] = sresult
            result['total'] = len(sresult)
            result['success'] = True
        except:
            self.driver.log(level='error')
            result['data'] = list()
            result['total'] = 0

        return result

if __name__ == '__main__':
    import json
    yd = Yandex()
    # yd = Yandex(driver='chrome', driver_path='/home/khoir/Workspace/chromedriver')
    # print json.dumps(yd.search(url='https://i.ytimg.com/vi/PiWDylHIoRk/maxresdefault.jpg', image=True), indent=4)
    print json.dumps(yd.search(file='/home/khoir/Pictures/getting-started-as-an-author-on-tes.png', image=True), indent=4)
    # print json.dumps(yd.search(file='/home/khoir/Pictures/getting-started-as-an-author-on-tes.png'), indent=4)