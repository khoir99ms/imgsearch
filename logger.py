__author__ = 'Khoir MS'

import sys
import logging

class Logger:

    def __init__(self, logname=None):
        self.__name = self.__class__.__name__
        if logname:
            self.__name = logname
        self.__setup_logging()

    def __remove_logger(self):
        logs = self.__logger.handlers
        for log in logs:
            self.__logger.removeHandler(log)
            log.flush()
            log.close()

    def log(self, *strings, **kwargs):
        strings = ['at {}'.format(sys._getframe(1).f_code.co_name), '-' if strings else ''] + list(strings)
        level = kwargs.pop('level', 'info').upper()
        self.__logger.log(getattr(logging, level), u' '.join(str(s) for s in strings), exc_info=(level=='ERROR'))

    def __setup_logging(self, level='INFO'):
        level = level.upper()
        self.__logger = logging.getLogger(self.__name)
        self.__logger.propagate = False
        self.__remove_logger()
        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(getattr(logging, level))

        # create formatter and add it to the handlers
        formatter = logging.Formatter('[%(asctime)s][%(name)s] - %(levelname)s - %(message)s', '%Y-%m-%d %H:%M:%S')
        ch.setFormatter(formatter)

        self.__logger.addHandler(ch)
        self.__logger.setLevel(level)